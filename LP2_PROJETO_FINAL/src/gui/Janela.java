package gui;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import produtorConsumidor.Produtor;
import produtorConsumidor.Consumidor;

public class Janela extends JFrame {

	private JPanel contentPane;
	private JTextField txtPorta_Produtor;
	private JTextField txtPorta_1;
	private JTextField txtPorta_2;
	private JTextField txtPorta_4;
	private JTextField txtPorta_3;
	private JTextField txtPorta_5;
	private JTextField txtPorta_6;
	private JTextField txtPorta_7;
	private JTextField txtPorta_8;
	private JTextField txtIP_1;
	private JTextField txtIP_2;
	private JTextField txtIP_3;
	private JTextField txtIP_4;
	private JTextField txtIP_5;
	private JTextField txtIP_6;
	private JTextField txtIP_7;
	private JTextField txtIP_8;
	
	Produtor produtor;
	Consumidor c1,c2,c3,c4,c5,c6,c7,c8;
	// play e pause a Thread, implementar posteriormente
	boolean btPay_c1 = false;
	boolean btPay_c2 = false;
	boolean btPay_c3 = false;
	boolean btPay_c4 = false;
	boolean btPay_c5 = false;
	boolean btPay_c6 = false;
	boolean btPay_c7 = false;
	boolean btPay_c8 = false;

	
	static int sizeBuffer = 20000;
	static List<byte[]> buffCompar = new ArrayList<byte[]>(sizeBuffer);

	static Semaphore empty = new Semaphore(sizeBuffer);
	static Semaphore full = new Semaphore(0);

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Janela frame = new Janela();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Janela() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 634, 580);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblProdutor = new JLabel("Produtor:");
		lblProdutor.setFont(new Font("Arial", Font.PLAIN, 14));
		lblProdutor.setBounds(10, 11, 164, 40);
		contentPane.add(lblProdutor);
		
		JLabel lbllConsumidores = new JLabel("Consumidores:");
		lbllConsumidores.setToolTipText("");
		lbllConsumidores.setFont(new Font("Arial", Font.PLAIN, 14));
		lbllConsumidores.setBounds(10, 91, 164, 40);
		contentPane.add(lbllConsumidores);
		
		txtPorta_Produtor = new JTextField();
		txtPorta_Produtor.setText("5000");
		txtPorta_Produtor.setToolTipText("Digite a porta do Produtor");
		txtPorta_Produtor.setBounds(88, 60, 86, 20);
		contentPane.add(txtPorta_Produtor);
		txtPorta_Produtor.setColumns(10);
		
		txtPorta_1 = new JTextField();
		txtPorta_1.setText("5001");
		txtPorta_1.setBounds(88, 142, 86, 20);
		contentPane.add(txtPorta_1);
		txtPorta_1.setColumns(10);
		
		txtPorta_2 = new JTextField();
		txtPorta_2.setText("5002");
		txtPorta_2.setColumns(10);
		txtPorta_2.setBounds(88, 192, 86, 20);
		contentPane.add(txtPorta_2);
		
		txtPorta_4 = new JTextField();
		txtPorta_4.setText("5003");
		txtPorta_4.setColumns(10);
		txtPorta_4.setBounds(88, 290, 86, 20);
		contentPane.add(txtPorta_4);
		
		txtPorta_3 = new JTextField();
		txtPorta_3.setText("5004");
		txtPorta_3.setColumns(10);
		txtPorta_3.setBounds(88, 242, 86, 20);
		contentPane.add(txtPorta_3);
		
		txtPorta_5 = new JTextField();
		txtPorta_5.setText("5005");
		txtPorta_5.setColumns(10);
		txtPorta_5.setBounds(88, 342, 86, 20);
		contentPane.add(txtPorta_5);
		
		txtPorta_6 = new JTextField();
		txtPorta_6.setText("5006");
		txtPorta_6.setColumns(10);
		txtPorta_6.setBounds(88, 392, 86, 20);
		contentPane.add(txtPorta_6);
		
		txtPorta_7 = new JTextField();
		txtPorta_7.setText("5007");
		txtPorta_7.setColumns(10);
		txtPorta_7.setBounds(88, 442, 86, 20);
		contentPane.add(txtPorta_7);
		
		txtPorta_8 = new JTextField();
		txtPorta_8.setText("5008");
		txtPorta_8.setColumns(10);
		txtPorta_8.setBounds(88, 490, 86, 20);
		contentPane.add(txtPorta_8);
		
		txtIP_1 = new JTextField();
		txtIP_1.setText("127.0.0.1");
		txtIP_1.setColumns(10);
		txtIP_1.setBounds(215, 142, 185, 20);
		contentPane.add(txtIP_1);
		
		txtIP_2 = new JTextField();
		txtIP_2.setText("127.0.0.1");
		txtIP_2.setColumns(10);
		txtIP_2.setBounds(215, 192, 185, 20);
		contentPane.add(txtIP_2);
		
		txtIP_3 = new JTextField();
		txtIP_3.setText("127.0.0.1");
		txtIP_3.setColumns(10);
		txtIP_3.setBounds(215, 242, 185, 20);
		contentPane.add(txtIP_3);
		
		txtIP_4 = new JTextField();
		txtIP_4.setText("127.0.0.1");
		txtIP_4.setColumns(10);
		txtIP_4.setBounds(215, 290, 185, 20);
		contentPane.add(txtIP_4);
		
		txtIP_5 = new JTextField();
		txtIP_5.setText("127.0.0.1");
		txtIP_5.setColumns(10);
		txtIP_5.setBounds(215, 342, 185, 20);
		contentPane.add(txtIP_5);
		
		txtIP_6 = new JTextField();
		txtIP_6.setText("127.0.0.1");
		txtIP_6.setColumns(10);
		txtIP_6.setBounds(215, 392, 185, 20);
		contentPane.add(txtIP_6);
		
		txtIP_7 = new JTextField();
		txtIP_7.setText("127.0.0.1");
		txtIP_7.setColumns(10);
		txtIP_7.setBounds(215, 442, 185, 20);
		contentPane.add(txtIP_7);
		
		txtIP_8 = new JTextField();
		txtIP_8.setText("127.0.0.1");
		txtIP_8.setColumns(10);
		txtIP_8.setBounds(215, 490, 185, 20);
		contentPane.add(txtIP_8);
		
		JCheckBox checkBoxALTA_1 = new JCheckBox("ALTA");
		checkBoxALTA_1.setBounds(426, 141, 68, 23);
		contentPane.add(checkBoxALTA_1);
		
		JCheckBox checkBoxALTA_2 = new JCheckBox("ALTA");
		checkBoxALTA_2.setBounds(426, 191, 68, 23);
		contentPane.add(checkBoxALTA_2);
		
		JCheckBox checkBoxALTA_4 = new JCheckBox("ALTA");
		checkBoxALTA_4.setBounds(426, 289, 68, 23);
		contentPane.add(checkBoxALTA_4);
		
		JCheckBox checkBoxALTA_3 = new JCheckBox("ALTA");
		checkBoxALTA_3.setBounds(426, 242, 68, 23);
		contentPane.add(checkBoxALTA_3);
		
		JCheckBox checkBoxALTA_5 = new JCheckBox("ALTA");
		checkBoxALTA_5.setBounds(426, 341, 68, 23);
		contentPane.add(checkBoxALTA_5);
		
		JCheckBox checkBoxALTA_6 = new JCheckBox("ALTA");
		checkBoxALTA_6.setBounds(426, 391, 68, 23);
		contentPane.add(checkBoxALTA_6);
		
		JCheckBox checkBoxALTA_7 = new JCheckBox("ALTA");
		checkBoxALTA_7.setBounds(426, 441, 68, 23);
		contentPane.add(checkBoxALTA_7);
		
		JCheckBox checkBoxALTA_8 = new JCheckBox("ALTA");
		checkBoxALTA_8.setBounds(426, 489, 68, 23);
		contentPane.add(checkBoxALTA_8);
		
		JButton btnPlay_1 = new JButton("Play");
		btnPlay_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if(!btPay_c1){
					btPay_c1 = true;

					c1 = new Consumidor(txtIP_1.getText(), Integer.parseInt(txtPorta_1.getText()), 1, buffCompar, empty, full);

					if(checkBoxALTA_1.isSelected()){
						c1.setPrioridade(true);
					}else{
						c1.setPrioridade(false);
					}
					
					c1.start();
					btnPlay_1.setText("Pause");
					txtIP_1.setEditable(false);
					txtPorta_1.setEditable(false);
					c1.setPausedConsumidor(false);

				}else{
					btPay_c1 = false;
					btnPlay_1.setText("Play");
					c1.setPausedConsumidor(true);
					checkBoxALTA_1.setEnabled(false);
					btnPlay_1.setEnabled(false);
				}

			}
		});
		btnPlay_1.setBounds(519, 141, 89, 23);
		contentPane.add(btnPlay_1);
		
		JButton btnPlay_2 = new JButton("Play");
		btnPlay_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(!btPay_c2){
					btPay_c2 = true;

					c2 = new Consumidor(txtIP_2.getText(), Integer.parseInt(txtPorta_2.getText()), 2, buffCompar, empty, full);

					if(checkBoxALTA_2.isSelected()){
						c2.setPrioridade(true);
					}else{
						c2.setPrioridade(false);
					}
					
					c2.start();
					btnPlay_2.setText("Pause");
					txtIP_2.setEditable(false);
					txtPorta_2.setEditable(false);
					c2.setPausedConsumidor(false);

				}else{
					btPay_c2 = false;
					btnPlay_2.setText("Play");
					c2.setPausedConsumidor(true);
					checkBoxALTA_2.setEnabled(false);
					btnPlay_2.setEnabled(false);
				}
				
			}
		});
		btnPlay_2.setBounds(519, 191, 89, 23);
		contentPane.add(btnPlay_2);
		
		JButton btnPlay_3 = new JButton("Play");
		btnPlay_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(!btPay_c3){
					btPay_c3 = true;

					c3 = new Consumidor(txtIP_3.getText(), Integer.parseInt(txtPorta_3.getText()), 3, buffCompar, empty, full);

					if(checkBoxALTA_3.isSelected()){
						c3.setPrioridade(true);
					}else{
						c3.setPrioridade(false);
					}
					
					c3.start();
					btnPlay_3.setText("Pause");
					txtIP_3.setEditable(false);
					txtPorta_3.setEditable(false);
					c3.setPausedConsumidor(false);

				}else{
					btPay_c3 = false;
					btnPlay_3.setText("Play");
					c3.setPausedConsumidor(true);
					checkBoxALTA_3.setEnabled(false);
					btnPlay_3.setEnabled(false);
				}
				
			}
		});
		btnPlay_3.setBounds(519, 241, 89, 23);
		contentPane.add(btnPlay_3);
		
		JButton btnPlay_4 = new JButton("Play");
		btnPlay_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(!btPay_c4){
					btPay_c4 = true;

					c4 = new Consumidor(txtIP_4.getText(), Integer.parseInt(txtPorta_4.getText()), 4, buffCompar, empty, full);

					if(checkBoxALTA_4.isSelected()){
						c4.setPrioridade(true);
					}else{
						c4.setPrioridade(false);
					}
					
					c4.start();
					btnPlay_4.setText("Pause");
					txtIP_4.setEditable(false);
					txtPorta_4.setEditable(false);
					c4.setPausedConsumidor(false);

				}else{
					btPay_c4 = false;
					btnPlay_4.setText("Play");
					c4.setPausedConsumidor(true);
					checkBoxALTA_4.setEnabled(false);
					btnPlay_4.setEnabled(false);
				}
				
			}
		});
		btnPlay_4.setBounds(519, 289, 89, 23);
		contentPane.add(btnPlay_4);
		
		JButton btnPlay_5 = new JButton("Play");
		btnPlay_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(!btPay_c5){
					btPay_c5 = true;

					c5 = new Consumidor(txtIP_5.getText(), Integer.parseInt(txtPorta_5.getText()), 5, buffCompar, empty, full);

					if(checkBoxALTA_5.isSelected()){
						c5.setPrioridade(true);
					}else{
						c5.setPrioridade(false);
					}
					
					c5.start();
					btnPlay_5.setText("Pause");
					txtIP_5.setEditable(false);
					txtPorta_5.setEditable(false);
					c5.setPausedConsumidor(false);

				}else{
					btPay_c5 = false;
					btnPlay_5.setText("Play");
					c5.setPausedConsumidor(true);
					checkBoxALTA_5.setEnabled(false);
					btnPlay_5.setEnabled(false);
				}
				
			}
		});
		btnPlay_5.setBounds(519, 341, 89, 23);
		contentPane.add(btnPlay_5);
		
		JButton btnPlay_6 = new JButton("Play");
		btnPlay_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(!btPay_c6){
					btPay_c6 = true;

					c6 = new Consumidor(txtIP_6.getText(), Integer.parseInt(txtPorta_6.getText()), 6, buffCompar, empty, full);

					if(checkBoxALTA_6.isSelected()){
						c6.setPrioridade(true);
					}else{
						c6.setPrioridade(false);
					}
					
					c6.start();
					btnPlay_6.setText("Pause");
					txtIP_6.setEditable(false);
					txtPorta_6.setEditable(false);
					c6.setPausedConsumidor(false);

				}else{
					btPay_c6 = false;
					btnPlay_6.setText("Play");
					c6.setPausedConsumidor(true);
					checkBoxALTA_6.setEnabled(false);
					btnPlay_6.setEnabled(false);
				}
				
			}
		});
		btnPlay_6.setBounds(519, 391, 89, 23);
		contentPane.add(btnPlay_6);
		
		JButton btnPlay_7 = new JButton("Play");
		btnPlay_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(!btPay_c7){
					btPay_c7 = true;

					c7 = new Consumidor(txtIP_7.getText(), Integer.parseInt(txtPorta_7.getText()), 7, buffCompar, empty, full);

					if(checkBoxALTA_7.isSelected()){
						c7.setPrioridade(true);
					}else{
						c7.setPrioridade(false);
					}
					
					c7.start();
					btnPlay_7.setText("Pause");
					txtIP_7.setEditable(false);
					txtPorta_7.setEditable(false);
					c7.setPausedConsumidor(false);

				}else{
					btPay_c7 = false;
					btnPlay_7.setText("Play");
					c7.setPausedConsumidor(true);
					checkBoxALTA_7.setEnabled(false);
					btnPlay_7.setEnabled(false);
				}
				
			}
		});
		btnPlay_7.setBounds(519, 441, 89, 23);
		contentPane.add(btnPlay_7);
		
		JButton btnPlay_8 = new JButton("Play");
		btnPlay_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(!btPay_c8){
					btPay_c8 = true;

					c8 = new Consumidor(txtIP_8.getText(), Integer.parseInt(txtPorta_8.getText()), 8, buffCompar, empty, full);

					if(checkBoxALTA_8.isSelected()){
						c8.setPrioridade(true);
					}else{
						c8.setPrioridade(false);
					}
					
					c8.start();
					btnPlay_8.setText("Pause");
					txtIP_8.setEditable(false);
					txtPorta_8.setEditable(false);
					c8.setPausedConsumidor(false);

				}else{
					btPay_c8 = false;
					btnPlay_8.setText("Play");
					c8.setPausedConsumidor(true);
					checkBoxALTA_8.setEnabled(false);
					btnPlay_8.setEnabled(false);
				}
				
			}
		});
		btnPlay_8.setBounds(519, 489, 89, 23);
		contentPane.add(btnPlay_8);
		
		JButton btnPlay_Produtor = new JButton("Play");
		btnPlay_Produtor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				btnPlay_Produtor.setEnabled(false);
				txtPorta_Produtor.setEnabled(false);
				
				produtor = new Produtor(Integer.parseInt(txtPorta_Produtor.getText()), buffCompar, empty, full);
				produtor.start();
				
				btnPlay_Produtor.setText("Active");

			}
		});
		btnPlay_Produtor.setBounds(184, 59, 89, 23);
		contentPane.add(btnPlay_Produtor);
	}
}
