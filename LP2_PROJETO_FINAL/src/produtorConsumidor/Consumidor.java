package produtorConsumidor;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.List;
import java.util.concurrent.Semaphore;

public class Consumidor extends Thread {

	private int idConsumidor;
	private List<byte[]> buffer;
	Semaphore empty;
	Semaphore full;
	static int front = 0;
	static Semaphore mutexF = new Semaphore(1); // para exclusao mutua

	private int nr = 0; // numero de leitores ativos
	private int nw = 0; // numero de escritores ativos
	//.....................RW: (nr==0 or nw==0) and nw <= 1

	static Semaphore e = new Semaphore(1); // Controla entrada na secao critica
	static Semaphore r = new Semaphore(0); // Atrasar os leitores(baixa prioridade)
	static Semaphore w = new Semaphore(0); // Atrasar os escritores(alta prioridade)

	private int dr = 0; // numero de leitores atrasados
	private int dw = 0; // numero de escritores atrasados

	boolean prioridade = false; // true => alta, false => baixa

	static boolean statusPrioridade = false;
	static int qtd_consumidores = 0;

	static byte data[] = new byte[1316];
	private DatagramPacket sendPacket;
	private DatagramSocket[] socket = new DatagramSocket[10]; // terei no maximo 10 consumidores

	private String IP; // IP consumidor
	private int PORTA; // PORTA consumidor (VLC)

	private int qtd_baixas = 0; // quantidade de consumidores baixa prioridade

	private boolean pausedConsumidor = false;


	public Consumidor(String IP, int PORTA, int idConsumidor, List<byte[]> buffer, Semaphore empty, Semaphore full) {
		this.IP = IP;
		this.PORTA = PORTA;
		this.idConsumidor = idConsumidor;
		this.buffer = buffer;
		this.empty = empty;
		this.full = full;
		this.prioridade = false;
		qtd_consumidores++;
		try { // dando erro, "prota j� em uso"
			// porta deve ser diferente, usada para pegar os dados
			this.socket[idConsumidor] = new DatagramSocket();
		} catch (SocketException e) {
			e.printStackTrace();
		}
	}

	public void run() {
		/* Foundations of Multithreaded, Parallel, 
		 * and Distributed Programming - Gregory R. Andrews
		 * Pag. 174
		 * 
		 * Passagem de bastao se baixa, se nao alta.
		 */
		if (!prioridade) {// baixa prioridade
			while (true) {
				try {
					e.acquire();
					if (nw > 0) {// se ha escritores
						dr = dr + 1;
						e.release();// libera entrada
						r.acquire();// leitor dorme
					}
					nr = nr + 1;
					SIGNAL();
					/* Acesso a base
					 * Solucao produtor consumidor em ConsumirBufferBaixa()
					 *  */
					ConsumirBufferBaixa(); 

					e.acquire();
					nr = nr - 1;
					SIGNAL();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		} else if (prioridade) {// alta prioridade
			while (true) {
				try {// <await(nr==0 and nw ==0) nw = nw+1;>
					e.acquire();
					if (nr > 0 || nw > 0) {// se h� leitores ou escritores
						dw = dw + 1;
						e.release();// libera entrada
						w.acquire();// escritor dorme
					}
					nw = nw + 1;
					SIGNAL();
					/* Acesso a base
					 * Solucao produtor/consumidor em ConsumirBufferAlta()
					 *  */
					ConsumirBufferAlta(); // ler da base

					e.acquire();
					nw = nw - 1;
					SIGNAL();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void SIGNAL() {
		if (nr == 0 && dr > 0) {
			dr = dr - 1;
			r.release(); // acorde um leitor(baixa)
		} else if ((nr == 0 && nw == 0) && dw > 0) {
			dw = dw - 1;
			w.release(); // acorde um escritor(alta)
		} else {
			e.release(); // libere a entrada(entrada)
		}

	}

	public void ConsumirBufferBaixa() {
		/* Foundations of Multithreaded, Parallel, 
		 * and Distributed Programming - Gregory R. Andrews
		 * Pag. 163
		 * 
		 * Solucao :: Produtor/Consumidor, 
		 * nesse caso "consumidor" e de Baixa priotidade (leitor)
		 * E com buffer limitado
		 */
		while (true) {
			try {
				full.acquire();		// protocolo entrada
				mutexF.acquire(); 	// exlusao mutua
				// FLAG pro produtor saber se � ALTA(true) ou BAIXA(false)
				statusPrioridade = false;
				// Se remover isto, da: java.lang.IndexOutOfBoundsException...
				while (front >= buffer.size()) { 
					// Aguarde at� que tenha dado suficiente pra ler
					sleep(1);
				}
				data = buffer.get(front);
				sendPacket = new DatagramPacket(data, data.length, InetAddress.getByName(IP), PORTA);
				socket[idConsumidor].send(sendPacket);
				qtd_baixas++; // controle de quantas j� leu o pacote
				/* para garantir que todos os consumidores de baixa prioridade
				 * consumam o mesmo pacote.
				 * Atualizo o "front" s� quando todas tiverem lido
				 * */
				if (qtd_baixas == qtd_consumidores) {
					front = (front + 1) % teste.sizeBuffer;
					qtd_baixas = 0; // zera para uma proxima rodada
				}


				mutexF.release();  //
				empty.release();   // Procolo de saida

				// Pausa/Play Thread
				if(this.pausedConsumidor) break;

			} catch (InterruptedException | IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void ConsumirBufferAlta() {
		/* Foundations of Multithreaded, Parallel, 
		 * and Distributed Programming - Gregory R. Andrews
		 * Pag. 163
		 * 
		 * Solucao :: Produtor/Consumidor, 
		 * nesse caso "consumidor" e de Baixa priotidade (leitor)
		 * E com buffer limitado
		 */
		while (true) {
			try {
				full.acquire();		// protocolo entrada
				mutexF.acquire(); 	// exlusao mutua

				// FLAG pro produtor saber se � ALTA(true) ou BAIXA(false)
				statusPrioridade = true;

				// Se remover isto, da: java.lang.IndexOutOfBoundsException...
				while (front >= buffer.size()) { 
					// Aguarde at� que tenha dado suficiente pra ler
					sleep(1);
				}

				data = buffer.get(front);
				sendPacket = new DatagramPacket(data, data.length, InetAddress.getByName(IP), PORTA);
				socket[idConsumidor].send(sendPacket);

				front = (front + 1) % teste.sizeBuffer;

				mutexF.release();
				empty.release(); // protocolo de saida

				// Pausa/Play Thread
				if(this.pausedConsumidor) break;


			} catch (InterruptedException | IOException e) {
				e.printStackTrace();
			}

		}
	}

	public String toStringPrioridade(boolean prioridade) {
		// funcao utilizada durante alguns testes...
		if (prioridade) {
			return "Alta";
		} else if (!prioridade) {
			return "Baixa";
		}
		return null;
	}

	public boolean isPrioridade() {
		return prioridade;
	}

	public void setPrioridade(boolean prioridade) {
		this.prioridade = prioridade;
	}

	public boolean isPausedConsumidor() {
		return pausedConsumidor;
	}

	public void setPausedConsumidor(boolean pausedConsumidor) {
		this.pausedConsumidor = pausedConsumidor;
	}

}