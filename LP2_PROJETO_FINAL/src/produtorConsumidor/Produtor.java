package produtorConsumidor;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.List;
import java.util.concurrent.Semaphore;

public class Produtor extends Thread {

	private List<byte[]> buffer; // buffer limitado, tamanho é passado em teste.
	static int rear = 0;

	Semaphore empty;
	Semaphore full;

	byte data[] = new byte[1316];
	private DatagramPacket receivePacket;
	private DatagramSocket socket;


	public Produtor(int PORTA, List<byte[]> buffer, Semaphore empty, Semaphore full) {
		try {
			this.socket = new DatagramSocket(PORTA);
		} catch (SocketException e) {e.printStackTrace();}
		this.buffer = buffer;
		this.empty = empty;
		this.full = full;
	}

	public void run() {
		/* Foundations of Multithreaded, Parallel, 
		 * and Distributed Programming - Gregory R. Andrews
		 * Pag. 162
		 * 
		 * Solucao :: Produtor/Consumidor, nesta caso "Produtor"
		 */
		while (true) {
			try {
				
				//recebe pacotes da PORTA
				this.receivePacket = new DatagramPacket(data, data.length); 
				socket.receive(receivePacket);

				empty.acquire(); 	// protocolo de entrada
				
				buffer.add(rear, receivePacket.getData());
				rear = (rear + 1) % teste.sizeBuffer;
				
				// Trava e libera de acordo com a prioridade
				prioridadeTrava_release(); 
				
			} catch (InterruptedException | IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void prioridadeTrava_release(){
		//se alta, chama 1, 
		//se baixa, chama todos de baixa.
		if(Consumidor.statusPrioridade){
			full.release();
		}else if(Consumidor.qtd_consumidores > 0){
			full.release(Consumidor.qtd_consumidores);
		}else{
			//prevenção de trava, 
			//caso não entre em nenhum anterior, libera mesmo assim
			full.release();
		}
	}

}