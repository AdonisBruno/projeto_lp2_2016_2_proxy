package produtorConsumidor;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class teste {

	static int sizeBuffer = 10000;
	static List<byte[]> buffCompar = new ArrayList<byte[]>(sizeBuffer);

	static Semaphore empty = new Semaphore(sizeBuffer);
	static Semaphore full = new Semaphore(0);

	public static void main(String[] args) {

		// escuto na porta 5000
		Produtor p1 = new Produtor(5000, buffCompar, empty, full);

		// envio na porta de destino cada cliente
		Consumidor c1 = new Consumidor("127.0.0.1", 5010, 1, buffCompar, empty, full);

		p1.start();
		c1.start();

	}

}
